/**
 * linalg.h
 * --------
 *
 * provide functionality of a sparse matrix in diagonal format
 *
 * @author ilhan oezgen, wahyd, ilhan.ozgen@wahyd.tu-berlin.de
 * @date 15 apr 17
 **/

#ifndef __LINALG_INCLUDED__
#define __LINALG_INCLUDED__

void put(struct diag*, int, int, double);

double get(struct diag*, int, int);

void multiply(struct diag*, double* v, double* av);

double dot(double*, double*, int);

double norm(double*, int);

#endif
