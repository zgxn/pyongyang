#ifndef __GLOBAL_INCLUDED__
#define __GLOBAL_INCLUDED__

/**
 * global.h
 * --------
 *
 * purpose: define global structs and parameters
 *
 * @author ilhan oezgen, www.wahyd.tu-berlin.de
 * @date   27 apr 17
 **/


/**
 * define a diagonal matrix -- functionality of the 
 * matrix is implemented in diagonal.c
 *
 * len_c : number of columns in original matrix
 * len_r : number of columns in original matrix
 **/
struct diag {

  double* values;
  int* ids;

  int len_c;
  int len_r;
  int len_d;
  int len_id;

};

#endif
