#!/usr/bin/env python

#
#
# plot results
#
#


import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

mpl.style.use('classic')

q  = np.flipud(np.genfromtxt('head.dat', skip_header=2))
vx = np.flipud(np.genfromtxt('velx.dat', skip_header=2))
vy = np.flipud(np.genfromtxt('vely.dat', skip_header=2))

x = np.linspace(-1.0, 1.0, len(q[:]))
y = np.linspace(-1.0, 1.0, len(q))

vnorm = np.zeros_like(vx)

for i in range(len(vx)):
	for j in range(len(vx[:])):
		vnorm[i,j] = np.sqrt(vx[i,j]**2 + vy[i,j]**2)

X, Y = np.meshgrid(x, y)

fig = plt.figure()
ax  = fig.add_subplot(121)
ax.set_aspect('equal')

cf = ax.contourf(x, y, q)
cs = ax.contour(x, y, q, colors='k')

ax.plot([-0.2, -0.2, 0.2, 0.2, -0.2], [-0.2, 0.2, 0.2, -0.2, -0.2], 'k-')

ax.clabel(cs, fontsize=9, inline=1)
ax.set_xlabel('x (m)')
ax.set_ylabel('y (m)')
ax.set_title('hydraulic head (m)')

plt.colorbar(cf, ax=ax, orientation='horizontal', shrink=0.8)

ax2 = fig.add_subplot(122)
ax2.set_aspect('equal')

cf2 = ax2.contourf(x, y, vnorm)
cs2 = ax2.contour(x, y, vnorm, colors='k')

ax2.quiver(x, y, vx, vy)

#ax2.clabel(cs2, fontsize=9, inline=1)
ax2.set_xlabel('x (m)')
ax2.set_ylabel('y (m)')
ax2.set_title('Darcy velocity (m/s)')


ax2.plot([-0.2, -0.2, 0.2, 0.2, -0.2], [-0.2, 0.2, 0.2, -0.2, -0.2], 'k-')

plt.tight_layout()
plt.colorbar(cf2, ax=ax2, orientation='horizontal', shrink=0.9)

plt.show()
#plt.savefig('results.png', dpi=200, format='png')
