/**
 * linalg.c
 * --------
 *
 * purpose: provide some algebra functions
 *
 * @author ilhan oezgen, wahyd, ilhan.oezgen@wahyd.tu-berlin.de
 * @date 15 apr 17
 * @date  3 may 17
 **/

// ----------------------------------------------------------------------------

#include <stdlib.h>
#include <math.h>
#include <omp.h>

#include "global.h"
#include "linalg.h"

// ----------------------------------------------------------------------------

/**
 * put a val into the diag matrix dia
 **/
void put(struct diag* dia, int r, int c, double val)
{

	int len_ids = dia->len_id;
	int len_d   = dia->len_d;

	int id = c - r;

	for (int i = 0; i < len_ids; i ++) {
		if (dia->ids[i] == id) {
			dia->values[i * len_d + r] = val;
			break;
		}
	}

}

/**
 * get a value (r,c) from diag matrix dia
 **/
double get(struct diag* dia, int r, int c)
{

	int len_ids =  dia->len_id;
	int len_d   =  dia->len_d;

	int id = c - r;
	double val = 0.0;

	for (int i = 0; i < len_ids; i ++) {
		if (dia->ids[i] == id) {
			val = dia->values[i * len_d + r];
			break;
		}
	}

	return val;

}

/**
 * calculate matrix vector multiplication
 **/
void multiply(struct diag* dia, double* v, double* av)
{

	int len_c = dia->len_c;
	int len_r = dia->len_r;

	#pragma omp parallel for
	for (int i = 0; i < len_r; i ++) {
		for (int j = 0; j < len_c; j ++) {
			av[i] = av[i] + get(dia, i, j) * v[j];
		}
	}

}

/**
 * return the dot product of vectors v1 and v2
 **/
double dot(double* v1, double* v2, int len)
{
	double dot_product = 0.0;
	for (int i = 0; i < len; i ++) {
		dot_product = dot_product + v1[i] * v2[i];
	}
	return dot_product;
}

/**
 * return the L2-norm of the vector
 **/
double norm(double* v, int len)
{
	return sqrt(dot(v, v, len));
}

