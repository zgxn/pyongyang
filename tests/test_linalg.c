/**
 * test_linalg.c
 * -------------
 *
 * purpose: test methods implemented in linalg.c
 *
 * @author ilhan oezgen, wahyd, ilhan.oezgen@wahyd.tu-berlin.de
 * @date   28 apr 17
 **/

#include <stdio.h>
#include <stdlib.h>
#include <math.h> // for fabs

#include "global.h"
#include "linalg.h"

int main(void)
{

	int nd = 5; // length of diagonal
  int ni = 2; // number of non-zero diagonals

	double* a = (double*) calloc((nd * ni), sizeof(double));
	int* ids  = (int*) calloc(ni, sizeof(int));
	double* v = (double*) calloc(nd, sizeof(double));
	ids[0] = -1;
	ids[1] =  0;
	double* result = (double*) calloc(nd, sizeof(double));

	a[2] = -12.0;

	for (int i = 0; i < nd * ni; i ++) {
		if (i >= 5) {
			a[i] = (i + 1.0) * 0.5;
			v[i-5] = i;
		}
	}

	struct diag m = { a, ids, nd, nd, nd, ni }; // initialize struct

	double reference_m[5][5] = { { 3.0,   0.0, 0.0, 0.0, 0.0}, 
															 { 0.0,   3.5, 0.0, 0.0, 0.0},
															 { 0.0, -12.0, 4.0, 0.0, 0.0},
															 { 0.0,   0.0, 0.0, 4.5, 0.0},
															 { 0.0,   0.0, 0.0, 0.0, 5.0} };

	if (fabs(dot(v, v, nd) - 255.0) < 1.0e-12) {
			printf("\ntest passed\n");
	}

	if (fabs(norm(v, nd) - sqrt(255.0)) < 1.0e-12) {
			printf("\ntest passed\n");
	}

	for (int i = 0; i < nd; i ++) {
		for (int j = 0; j < nd; j ++) {
			if (fabs(get(&m, i, j) - reference_m[i][j]) < 1.0e-12) {
			printf("\ntest passed\n");
		  }
		}
	}

	put(&m, 2, 2, -100.0);
	reference_m[2][2] = 100.0;

	for (int i = 0; i < nd; i ++) {
		for (int j = 0; j < nd; j ++) {
			if (fabs(get(&m, i, j) - reference_m[i][j]) < 1.0e-12) {
			printf("\ntest passed\n");
		  }
		}
	}

	multiply(&m, v, result);

	double reference[5] = { 15.0, 21.0, -772.0, 36.0, 45.0 };

	for (int i = 0; i < 5; i ++) {
		if (fabs(result[i] - reference[i]) < 1.0e-12) {
			printf("\ntest passed\n");
		} else {
			printf("\ntest failed : multiply");
			printf("  expected : %g", reference[i]);
			printf("  but was  : %g", result[i]);
		}
	}

	printf("\n");

	free(result);
	free(a);
	free(ids);
	free(v);
}
