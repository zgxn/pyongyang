/**
 * test_pcg.c
 * ----------
 *
 * purpose: test solver.pcg()-method
 * cf. https://en.wikipedia.org/wiki/Conjugate_gradient_method
 * for the test case used
 **/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "global.h"
#include "linalg.h"
#include "solver.h"

int main (void)
{

	// init system
	double a[6] = { 0.0, 1.0, 4.0, 3.0, 1.0, 0.0 };
	int  ids[3] = { -1, 0, 1 };

	struct diag m  = { a, ids, 2, 2, 2, 3 };

	double c[2] = { 1.0 / 4.0, 1.0 / 3.0 };
	int cids[1] = { 0 };

	struct diag pre = { c, cids, 2, 2, 2, 1 }; // Jacobi preconditioner

	double b[3]    = { 1.0, 2.0 };
	double x0[2]   = { 20000.0, 10000.0 };

	pcg(&m, b, x0, &pre, 1.0e-12);

	printf(">>> x0: \n");
	for (int i = 0; i < 2; i ++) {
		printf(">>>  %g\n", x0[i]);
	}

	if (fabs(x0[0] - (1.0/11.0)) <= 1.0e-10) {
		printf(">>> test passed.\n");
	} else {
		printf(">>> test failed. expected: %g, found: %g\n", (1.0/11.0), x0[0]);
	}

	if (fabs(x0[1] - (7.0/11.0)) <= 1.0e-10) {
		printf(">>> test passed.\n");
	} else {
		printf(">>> test failed. expected: %g, found: %g\n", (7.0/11.0), x0[1]);
	}
}
