/**
 * solver.h
 * --------
 *
 * gradient-based iterative solvers for linear systems of equations
 * Ax = b
 *
 * the return value indicates convergence within max_iter (input) 
 * iterations (0), or no convergence within max_iter iterations (1).
 *
 * upon successful return, output arguments have the following 
 * values:
 *
 *   x0  : approximate solution to Ax = b
 *   tol : accuracy of the approximate solution
 *
 * @author ilhan oezgen, wahyd, ilhan.oezgen@wahyd.tu-berlin.de
 * @date 10 apr 17
 * @date 11 apr 17 : add norm function
 * @date 28 apr 17 : add pcg solver
 * @date  8 jun 17 : add bicgstab solver
 *
 **/

#ifndef __SOLVER_INCLUDED__
#define __SOLVER_INCLUDED__

int pcg(struct diag*, double*, double*, struct diag*, double);

int bic(struct diag*, double*, double*, struct diag*, double);

#endif
