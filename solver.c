/**
 * solver.c
 * --------
 *
 * gradient-based iterative solvers for linear systems of equations
 * Ax = b
 *
 * the return value indicates convergence within max_iter (input) 
 * iterations (0), or no convergence within max_iter iterations (1).
 *
 * upon successful return, output arguments have the following 
 * values:
 *
 *   x0  : approximate solution to Ax = b
 *   tol : accuracy of the approximate solution
 *
 * @author ilhan oezgen, wahyd, ilhan.oezgen@wahyd.tu-berlin.de
 * @date 10 apr 17
 * @date 11 apr 17 : add norm function
 * @date 28 apr 17 : add pcg solver
 * @date  8 jun 17 : add bicgstab solver
 *
 **/

// ----------------------------------------------------------------------------

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "global.h" // struct diag
#include "solver.h"
#include "linalg.h" // multiply, etc.

// ----------------------------------------------------------------------------

/*
 * preconditioned biconjugate gradient stabilized solver
 * m    : preconditioner matrix
 * inout: x0  (in: initial guess, out: final result)
 */
int bic(struct diag* a, double* b, double* x0, struct diag* m, double tol)
{

	int len_c = a->len_c;
	int count = 1;
	int flag  = 1;

	double* r      = (double*) calloc(len_c, sizeof(double));
	double* r_tld  = (double*) calloc(len_c, sizeof(double));
	double* p      = (double*) calloc(len_c, sizeof(double));
	double* v      = (double*) calloc(len_c, sizeof(double));
	double* p_hat  = (double*) calloc(len_c, sizeof(double));
	double* s      = (double*) calloc(len_c, sizeof(double));
	double* s_hat  = (double*) calloc(len_c, sizeof(double));
	double* t      = (double*) calloc(len_c, sizeof(double));

	double eps   = tol;
	double omega = 1.0;
	double rho   = 1.0;
	double rho_1 = 1.0;
	double beta  = 1.0;
	double alpha = 1.0;


	double bnrm = norm( b, len_c);

	if (fabs(bnrm) < tol) {
		bnrm = 1.0;
	}

	// r = b - A x
	multiply(a, x0, r);
	for (int i = 0; i < len_c; i ++) {
		r[i] = b[i] - r[i];
	}
	//

	eps = norm(r, len_c) / bnrm;

	if (eps < tol) {
		flag = 0;
		printf(">>> (bic) :: iteration complete w/ err : %g, \
			flag: %d, steps: %d\n", eps, flag, count);
		return flag;
	}

	memcpy(r_tld, r, len_c * sizeof(double));

	for (count = 1; count < 1000; count ++) {

		rho = dot(r_tld, r, len_c);

		if (rho == 0) {
			break;
		}

		if (count > 1) {
			beta = (rho / rho_1) * (alpha / omega);
			for (int j = 0; j < len_c; j ++) {
				p[j] = r[j] + beta * (p[j] - omega * v[j]);
			}
		} else {
			memcpy(p, r, len_c * sizeof(double));
		}

		memset(p_hat, 0.0, len_c * sizeof(double));
		multiply(m, p, p_hat);

		memset(v, 0.0, len_c * sizeof(double));
		multiply(a, p_hat, v);

		alpha = rho / dot(r_tld, v, len_c);
		for (int j = 0; j < len_c; j ++) {
			s[j] = r[j] - alpha * v[j];
		}

		double snrm = norm(s, len_c);

		if (snrm < tol) {
			for (int j = 0; j < len_c; j ++) {
				x0[j] = x0[j] + alpha * p_hat[j];
			}
			eps = snrm / bnrm;
			break;
		}

		memset(s_hat, 0.0, len_c * sizeof(double));
		multiply(m, s, s_hat);

		memset(t, 0.0, len_c * sizeof(double));
		multiply(a, s_hat, t);

		omega = dot(t, s, len_c) / dot(t, t, len_c);

		for (int j = 0; j < len_c; j ++) {
			x0[j] = x0[j] + alpha * p_hat[j] + omega * s_hat[j];
			r[j] = s[j] - omega * t[j];
		}

		eps = norm(r, len_c);

		if (eps < tol) {
			break;
		}

		if (fabs(omega) == 0) {
			break;
		}

		rho_1 = rho;

	}

	if (eps < tol) {
		flag = 0;
	} else if (omega == 0) {
		flag = -2;
	} else if (rho == 0) {
		flag = -1;
	} else {
		flag = 1;
	}

	printf(">>> (bic) :: iteration complete w/ err : %g, \
		flag: %d, steps: %d\n", eps, flag, count);

	free(r);
	free(r_tld);
	free(p);
	free(v);
	free(p_hat);
	free(s);
	free(s_hat);
	free(t);

	return flag;

}


/*
 * preconditioned conjugate gradient solver
 * symmetric matrices only
 * c    : preconditioner matrix
 * inout: x0  (in: initial guess, out: final result)
 */
int pcg(struct diag* a, double* b, double* x0, struct diag* c, double tol)
{

	// params
	int len_c = a->len_c;
	int count = 0;
	int flag  = 1;

	double* r0 = (double*) calloc(len_c, sizeof(double));
	double* r1 = (double*) calloc(len_c, sizeof(double));
	double* z0 = (double*) calloc(len_c, sizeof(double));
	double* z1 = (double*) calloc(len_c, sizeof(double));
	double* p  = (double*) calloc(len_c, sizeof(double));
	double* t  = (double*) calloc(len_c, sizeof(double));

	double alph = 0.0;
	double beta = 0.0;

	double eps = tol;


	// bootstrap
	multiply(a, x0, r0);

	for (int i = 0; i < len_c; i ++) {
		r0[i] = b[i] - r0[i];
	}

	multiply(c, r0, z0);


	memcpy(p, z0, len_c * sizeof(double));

	// loop
	while (count < 1000) {

		multiply(a, p, t);
		alph = dot(r0, z0, len_c) / dot(p, t, len_c);

		for (int i = 0; i < len_c; i ++) {
			x0[i] = x0[i] + alph * p[i];
			r1[i] = r0[i] - alph * t[i];
		}

		eps = norm(r1, len_c);

		if (eps < tol) {
			flag = 0;
			break;
		} else {
			multiply(c, r1, z1);
			beta = dot(z1, r1, len_c) / dot(z0, r0, len_c);

			for (int i = 0; i < len_c; i ++) {
				p[i] = z1[i] + beta * p[i];
			}

		memcpy(z0, z1, len_c * sizeof(double));
		memcpy(r0, r1, len_c * sizeof(double));

		}

		memset(t,  0.0, len_c * sizeof(double));
		memset(z1, 0.0, len_c * sizeof(double));
		memset(r1, 0.0, len_c * sizeof(double));

		count = count + 1;
	}

	free(z0);
	free(z1);
	free(p);
	free(r0);
	free(r1);
	free(t);

	printf(">>> (pcg) :: iteration complete w/ err : %g, \
		flag: %d, steps: %d\n", eps, flag, count);

	return flag;

}

