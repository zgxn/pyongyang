/**
 * system_matrix.h
 * ---------------
 *
 * solve 2 dimensional groundwater flow. the hydraulic head 
 * is calculated based on finite differences. 
 *
 * @author ilhan oezgen, wahyd, ilhan.oezgen@wahyd.tu-berlin.de
 * @date 6 apr 17
 *      10 apr 17 : switch to 1d vectors
 *
 **/

#ifndef __SYSMAT_INCLUDED__
#define __SYSMAT_INCLUDED__

void system_matrix(struct diag*, int, int, double*);

void boundary(int, int, int, int, int, int, int, double, double, double, 
	double, struct diag*, double*, double*);

void darcy(double*, double*, int, int, double, double, double*, double*);

double harmonic(double, double);

#endif
