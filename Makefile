# Makefile
#
# make           : build main program
# make clean     : delete main program and all object files
# make cleanall  : make clean and delete all results
# make plot      : build main program, run simulation, plot results

PRG=pyongyang
CC=cc
CFLAGS=-Wall -Wextra -pedantic -g
LFLAGS=-fopenmp
CPU=1
SRC=main.c linalg.c solver.c sysmat.c
RUNFLAGS=OMP_NUM_THREADS=$(CPU)
INCLUDES=
LIB=-lm

# -----------------------------------------------------------------------------

OBJ=$(SRC:.c=.o)

all: $(PRG)

plot: $(PRG)
	$(RUNFLAGS) ./$(PRG); cd result; python plot.py; cd ../

$(PRG): $(OBJ)
	$(CC) $(LFLAGS) $(CFLAGS) $(OBJ) -o $@ $(LIB) 

%.o: %.c
	$(CC) $(LFLAGS) $(CFLAGS) -c $< -o $@

clean:
	rm -f $(PRG) $(OBJ)

cleanall:
	make clean
	cd result; rm -f *.dat; rm -f *.png

.PHONY: all clean cleanall

# eof
