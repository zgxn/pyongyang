/**
 * sysmat.c
 * ---------------
 *
 * purpose : calculate the system matrix and return darcy velocities
 *
 * @author ilhan oezgen, wahyd, ilhan.oezgen@wahyd.tu-berlin.de
 * @date 6 apr 17
 *      10 apr 17 : switch to 1d vectors
 *
 **/

// ----------------------------------------------------------------------------

#include <stdlib.h>
#include <stdio.h>

#include "global.h"
#include "sysmat.h"
#include "linalg.h"

// ----------------------------------------------------------------------------

/**
 * build system matrix
 *
 * inout:
 * struct diag m : system matrix
 **/
void system_matrix(struct diag* m, int nx, int ny, double* val)
{

	// the system matrix is a square matrix with following dimension: 
	int dim = nx * ny;

	// loop over inner cells with 4 neighbors
	for (int i = 1; i < ny - 1; i ++) {
		for (int j = 1; j < nx - 1; j ++) {

			int ik = i * nx + j;
			int i1 = ik - 1;
			int i2 = (i - 1) * nx + j;
			int i3 = ik + 1;
			int i4 = (i + 1) * nx + j;

			double val1 = harmonic(val[ik], val[i1]);
			double val2 = harmonic(val[ik], val[i2]);
			double val3 = harmonic(val[ik], val[i3]);
			double val4 = harmonic(val[ik], val[i4]);

			put(m, ik, ik,   val1 + val2 + val3 + val4);
			put(m, ik, i1, - val1);
			put(m, ik, i2, - val2);
			put(m, ik, i3, - val3);
			put(m, ik, i4, - val4);

		}
	}

	// boundary cells with 2 neighbors

	// upper left corner : r = 0, c = 0;
	double kb1 = harmonic(val[0], val[1]);
	double kb2 = harmonic(val[0], val[nx]);

	put(m, 0, 0,    kb1 + kb2);
	put(m, 0, 1,  - kb1);
	put(m, 0, nx, - kb2);

	// upper right corner : r = 0, c = nx
	kb1 = harmonic(val[nx - 1], val[nx - 2]);
	kb2 = harmonic(val[nx - 1], val[2 * nx - 1]);

	put(m, nx - 1, nx - 1,       kb1 + kb2);
	put(m, nx - 1, nx - 2,     - kb1);
	put(m, nx - 1, 2 * nx - 1, - kb2);


	// lower left corner : r = ny, c = 0
	kb1 = harmonic(val[dim - nx], val[dim - nx + 1]);
	kb2 = harmonic(val[dim - nx], val[dim - 2 * nx]);

	put(m, dim - nx, dim - nx,       kb1 + kb2);
	put(m, dim - nx, dim - nx + 1, - kb1);
	put(m, dim - nx, dim - 2 * nx, - kb2);

	// lower right corner
	kb1 = harmonic(val[dim - 1], val[dim - 2]);
	kb2 = harmonic(val[dim - 1], val[dim - 1 - nx]);

	put(m, dim - 1, dim - 1,        kb1 + kb2);
	put(m, dim - 1, dim - 2,      - kb1);
	put(m, dim - 1, dim - 1 - nx, - kb2);


	// boundary cells with 3 neighbors

	// additional value required
	double kb3 = 0.0;

	// upper boundary (i = 0)
	for (int j = 1; j < nx - 1; j ++) {
		kb1 = harmonic(val[j], val[j - 1]);
		kb2 = harmonic(val[j], val[j + 1]);
		kb3 = harmonic(val[j], val[nx + j]);

		put(m, j, j,        kb1 + kb2 + kb3);
		put(m, j, j - 1,  - kb1);
		put(m, j, j + 1,  - kb2);
		put(m, j, nx + j, - kb3);

	}

	// lower boundary (i = n_max)
	for (int j = 1; j < nx - 1; j ++) {

		int c = dim - nx;

		kb1 = harmonic(val[c + j], val[c + j - 1]);
		kb2 = harmonic(val[c + j], val[c + j + 1]);
		kb3 = harmonic(val[c + j], val[dim - 2 * nx + j]);

		put(m, c + j, c + j,              kb1 + kb2 + kb3);
		put(m, c + j, c + j - 1,        - kb1);
		put(m, c + j, c + j + 1,        - kb2);
		put(m, c + j, dim - 2 * nx + j, - kb3);

	}

	// left boundary (j = 0)
	for (int i = 1; i < ny - 1; i ++) {

		int c = i * nx;

		kb1 = harmonic(val[c], val[(i - 1) * nx]);
		kb2 = harmonic(val[c], val[(i + 1) * nx]);
		kb3 = harmonic(val[c], val[i * nx + 1]);

		put(m, c, c,              kb1 + kb2 + kb3);
		put(m, c, (i - 1) * nx, - kb1);
		put(m, c, (i + 1) * nx, - kb2);
		put(m, c, i * nx + 1,   - kb3);

	}

	// right boundary (j = n_max)
	for (int i = 1; i < ny - 1; i ++) {

		int c = (i + 1) * nx - 1;

		kb1 = harmonic(val[c], val[i * nx - 1]);
		kb2 = harmonic(val[c], val[(i + 2) * nx - 1]);
		kb3 = harmonic(val[c], val[i * nx + nx - 2]);

		put(m, c, c,                  kb1 + kb2 + kb3);
		put(m, c, i * nx - 1,       - kb1);
		put(m, c, (i + 2) * nx - 1, - kb2);
		put(m, c, i * nx + nx - 2,  - kb3);

	}

}


/**
 * set boundary conditions
 *
 * inout: m
 **/
void boundary(int dim, int nx, int ny, int north, int south, int east, 
	int west, double val_north, double val_south, double val_east, 
	double val_west, struct diag* m, double* r, double* x)
{

	// 0: dirichlet | 1: neumann | 2: closed wall



	if (north == 0) {

		for (int i = 0; i < nx;  i ++) {
			x[i] = val_north;
		}

		double* ar = (double*) calloc(dim, sizeof(double));

		multiply(m, x, ar);

		for (int i = 0; i < dim; i ++) {
			r[i] = r[i] - ar[i];
			if (i < nx) {
				r[i] = x[i];
			}
		}

		free(ar);

		for (int i = 0; i < nx; i ++) {
			for (int j = 0; j < dim; j ++) {
				if (i != j) {
					put(m, j, i, 0.0);
					put(m, i, j, 0.0);
				} else {
					put(m, j, i, 1.0);
				}
			}
		}

	} else if (north == 1) {

		for (int i = 0; i < nx; i ++) {
			r[i] = val_north;
		}

	}

	if (south == 0) {

		for (int i = dim - nx; i < dim; i ++) {
			x[i] = val_south;
		}

		double* ar = (double*) calloc(dim, sizeof(double));

		multiply(m, x, ar);

		for (int i = 0; i < dim; i ++) {
			r[i] = r[i] - ar[i];
			if ((i >= dim - nx) && (i < dim)) {
				r[i] = x[i];
			}
		}

		free(ar);

		for (int i = dim - nx; i < dim; i ++) {
			for (int j = 0; j < dim; j ++) {
				if (i != j) {
					put(m, j, i, 0.0);
					put(m, i, j, 0.0);
				} else {
					put(m, j, i, 1.0);
				}
			}
		}

	} else if (south == 1) {

		for (int i = dim - nx; i < dim; i ++) {
			r[i] = val_south;
		}

	}

}

/**
 * compute darcy velocities
 *
 * inout: vx, vy
 **/
void darcy(double* h, double* kf, int nx, int ny, 
	double dx, double dy, double* vx, double* vy)
{

	// central differencing inside the domain
	for (int i = 1; i < ny - 1; i ++) {
		for (int j = 1; j < nx - 1; j ++) {
			vx[i * nx + j] = - harmonic(kf[i * nx + j - 1], kf[i * nx + j + 1]) 
			  * (h[i * nx + j + 1] - h[i * nx + j - 1]) / (2.0 * dx);
			vy[i * nx + j] = - harmonic(kf[(i + 1) * nx + j], kf[(i - 1) * nx + j]) 
			  * (h[(i - 1) * nx + j] - h[(i + 1) * nx + j]) / (2.0 * dy);
		}
	}

	// left boundary, j = 0
	for (int i = 1; i < ny - 1; i ++) {
		vx[i * nx] = - harmonic(kf[i * nx + 1], kf[i * nx]) * 
			(h[i * nx + 1] - h[i * nx]) / dx;
		vy[i * nx] = - harmonic(kf[(i + 1) * nx], kf[(i - 1) * nx]) 
			* (h[(i - 1) * nx] - h[(i + 1) * nx]) / (2.0 * dy);
	}

	// right boundary, j = nx - 1
	for (int i = 1; i < ny - 1; i ++) {
		vx[i * nx + (nx - 1)] = - harmonic(kf[i * nx + (nx - 2)], 
			kf[i * nx + (nx - 1)]) 
			* (h[i * nx + (nx - 1)] - h[i * nx + (nx - 2)]) / dx;
		vy[i * nx + (nx - 1)] = - harmonic(kf[(i + 1) * nx + (nx - 1)], 
			kf[(i - 1) * nx + (nx - 1)]) 
			* (h[(i - 1) * nx + (nx - 1)] - h[(i + 1) * nx + (nx - 1)]) / (2.0 * dy);
	}

	// lower boundary, i = ny - 1
	for (int j = 1; j < nx - 1; j ++) {
		vx[(ny - 1) * nx + j] = - harmonic(kf[(ny - 1) * nx + j + 1], 
			kf[(ny - 1) * nx + j - 1]) * (h[(ny - 1) * nx + j + 1] - 
			h[(ny - 1) * nx + j - 1]) / (2.0 * dx);
		vy[(ny - 1) * nx + j] = - harmonic(kf[(ny - 1) * nx + j], 
			kf[(ny - 2) * nx + j]) * (h[(ny - 2) * nx + j] - 
			h[(ny - 1) * nx + j]) / dy;
		}

	// upper boundary, i = 0
	for (int j = 1; j < nx - 1; j ++) {
		vx[j] = - harmonic(kf[j + 1], kf[j - 1]) * (h[j + 1] - h[j - 1]) 
			/ (2.0 * dx);
		vy[j] = - harmonic(kf[j], kf[nx + j]) * (h[j] - h[nx + j]) / dy;
	}

	// corners
	vx[0] = - harmonic(kf[0], kf[1]) * (h[1] - h[0]) / dx;
	vy[0] = - harmonic(kf[0], kf[nx]) * (h[0] - h[nx]) / dy;

	vx[nx-1] = - harmonic(kf[nx-1], kf[nx-2]) * (h[nx-1] - h[nx-2]) / dx;
	vy[nx-1] = - harmonic(kf[nx-1], kf[2*nx-1]) * (h[nx-1] - h[2*nx-1]) / dy;

	vx[(ny-1)*nx] = - harmonic(kf[(ny-1)*nx], kf[(ny-1)*nx+1]) 
		* (h[(ny-1)*nx] - h[(ny-1)*nx+1]) / dx;
	vy[(ny-1)*nx] = - harmonic(kf[(ny-1)*nx], kf[(ny-2)*nx]) 
		* (h[(ny-1)*nx] - h[(ny-2)*nx]) / dy;

	vx[(ny-1)*nx + (nx-1)] = - harmonic(kf[(ny-1)*nx + (nx-1)], 
		kf[(ny-1)*nx + (nx-2)]) * (h[(ny-1)*nx + (nx-1)] - h[(ny-1)*nx + (nx-2)]) 
		/ dx;
	vy[(ny-1)*nx + (nx-1)] = - harmonic(kf[(ny-1)*nx + (nx-1)], 
		kf[(ny-2)*nx + (nx-1)]) * (h[(ny-1)*nx + (nx-1)] - h[(ny-2)*nx + (nx-1)]) 
		/ dy;

}

double harmonic(double ki, double kj)
{
	return 2.0 * ki * kj / (ki + kj);
}
